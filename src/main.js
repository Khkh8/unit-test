// src/main.js

const express = require('express');
const app = express();
const port = 3000;

const mylib = require('./mylib'); // .js not required
const unitTest = require('./unitTest') // importing new module

console.log({
    sum: mylib.sum(1, 1 ),
    random: mylib.random(),
    arrayGen: mylib.arrayGen(),
    stringCombiner: unitTest.stringCombiner("Pineapple", "Pizza")
});

app.get('/', (req, res) => {
    res.send('Hello world');
});

app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`);
});